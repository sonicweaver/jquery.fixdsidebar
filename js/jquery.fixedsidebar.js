/*!
 * jquery.fixdsidebar.js jQuery plug in Javascript
 * http://www.electric-fruits.com
 *
 *
 * Copyright 2016 Electric Fruits Norihiko Takami
 * Released under the MIT license
 * 
 *
 * Date: 2016-05-01T18:00
 */


$.fn.fixedsidebar = function() {
	
	  var wh = $(window).outerHeight();
	  var bb = $(this);
      var parent = $(this).parent();
      var parent_horize_posi = parent.get(0).offsetLeft;
	  var bbpt = bb.get(0).offsetTop;
	  var bbpl = bb.get(0).offsetLeft;
	  var scrollTop = $(this).scrollTop();
	  var scrollLeft = $(this).scrollLeft();
	  var bbh;
	  var bbw;
	  var vari_h_w_b;
	  var bodyminw;
      var horize_move;
      var bbpl_df = bbpl;
      var id;
	  
    
	
	$(window).on('load',function() {
	    bbh = bb.innerHeight();
		bbw = bb.innerWidth();
        bbpt = bb.get(0).offsetTop;
        bbpl = bb.get(0).offsetLeft;
		bodyminw = bbw + bbpl;
		$('body').css({'min-width':bodyminw +'px'});
		bb.css({'position':'fixed','left':bbpl + 'px','top':bbpt + 'px'});
	});
	
	$(window).on('resize',function() {
        wh = $(window).outerHeight();
        id = setTimeout(function(){
         horize_move = parent_horize_posi - (parent.get(0).offsetLeft);
         bbpl = bbpl_df-horize_move;
            bb.css({'left': bbpl + 'px'});
            clearTimeout(id);
        },2);
        
	});
	
	$(window).on('scroll',function() {
        
		
		scrollTop = $(this).scrollTop();
		scrollLeft = $(this).scrollLeft();
		$('body').css({'min-width':bodyminw + 'px'});
		
		if(scrollLeft > 0 ) {
			  bb.css({'left': bbpl - scrollLeft + 'px'});
            } else {
			bb.css({'left': bbpl + 'px'});
		}
		
		if(wh > (bbh+bbpt)) {
			if(scrollTop > bbpt) {
			 bb.css({'top': '0px'});
			} else {
			   bb.css({'top': -(scrollTop - bbpt) + 'px'});
			}
			
		} else {
			vari_h_w_b = wh - (bbh+bbpt);
			
			if(scrollTop > (Math.abs(vari_h_w_b)+bbpt)) {
				bb.css({'top': vari_h_w_b + 'px'});
                
                console.log("vari_h_w_b : " + vari_h_w_b);
                
			} else {
				bb.css({'top': -(scrollTop - bbpt) + 'px'});
                
			}
			
		}
	});
};